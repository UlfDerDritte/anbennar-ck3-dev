#k_the_borders
##d_gisden
###c_gisden
326 = {		#Gisden

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2179 = {

    # Misc
    holding = none

    # History

}
2180 = {

    # Misc
    holding = none

    # History

}

###c_autumnfield
920 = {		#Autumnfield

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2176 = {

    # Misc
    holding = none

    # History

}

###c_upsfield
321 = {		#Upsfield

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2177 = {

    # Misc
    holding = none

    # History

}
2178 = {

    # Misc
    holding = none

    # History

}

###c_quintenbury
312 = {		#Quintenbury

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2181 = {

    # Misc
    holding = none

    # History

}

##d_brinkwick
###c_brinkwick
322 = {		#Brinkwick

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2171 = {

    # Misc
    holding = none

    # History

}

###c_eastborders
324 = {		#Eastborders

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2174 = {

    # Misc
    holding = none

    # History

}
2175 = {

    # Misc
    holding = none

    # History

}

###c_murkglade
323 = {		#Murkglade

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2172 = {

    # Misc
    holding = none

    # History

}

###c_wisphollow
421 = {		#Wisphollow

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2173 = {

    # Misc
    holding = none

    # History

}

##d_arannen
###c_arannen
904 = {		#Arannen

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = castle_holding

    # History
}
2165 = {

    # Misc
    holding = none

    # History

}
2166 = {

    # Misc
    holding = none

    # History

}
2167 = {

    # Misc
    holding = none

    # History

}
2168 = {

    # Misc
    holding = none

    # History

}

###c_cronesford
325 = {		#Cronesford

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2169 = {

    # Misc
    holding = none

    # History

}
2170 = {

    # Misc
    holding = none

    # History

}

###c_endersby
313 = {		#Endersby

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2162 = {

    # Misc
    holding = none

    # History

}
2163 = {

    # Misc
    holding = none

    # History

}
2164 = {

    # Misc
    holding = none

    # History

}

###c_marllin
418 = {		#Marllin

    # Misc
    culture = milcori
    religion = court_of_nerat
	holding = castle_holding

    # History
}
2160 = {

    # Misc
    holding = none

    # History

}
2161 = {

    # Misc
    holding = none

    # History

}

##d_highcliff
###c_sorncell
413 = {		#Sorncell

    # Misc
    culture = milcori
    religion = court_of_nerat
	holding = castle_holding

    # History
}
2143 = {

    # Misc
    holding = none

    # History

}
2144 = {

    # Misc
    holding = none

    # History

}
2145 = {

    # Misc
    holding = none

    # History

}

###c_doveswalk
412 = {		#Doveswalk

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2141 = {

    # Misc
    holding = none

    # History

}
2142 = {

    # Misc
    holding = none

    # History

}

###c_bordercliff
921 = {		#Bordercliff

    # Misc
    culture = milcori
    religion = court_of_nerat
	holding = castle_holding

    # History
}
2146 = {

    # Misc
    holding = none

    # History

}
2147 = {

    # Misc
    holding = none

    # History

}

###c_aiscestir
922 = {		#Aiscestir

    # Misc
    culture = milcori
    religion = court_of_nerat
	holding = castle_holding

    # History
}
2148 = {

    # Misc
    holding = none

    # History

}

##d_antirhal
###c_antirhalan
416 = {		#Antirhalan

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2152 = {

    # Misc
    holding = none

    # History

}
2153 = {

    # Misc
    holding = none

    # History

}

###c_aenawick
420 = {		#Aenawick

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2149 = {

    # Misc
    holding = none

    # History

}
2150 = {

    # Misc
    holding = none

    # History

}
2151 = {

    # Misc
    holding = none

    # History

}

###c_varlosen
419 = {		#Varlosen

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2158 = {

    # Misc
    holding = none

    # History

}
2159 = {

    # Misc
    holding = none

    # History

}

##d_tellum
###c_tellum
417 = {

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2155 = {

    # Misc
    holding = none

    # History

}
2156 = {

    # Misc
    holding = none

    # History

}
2157 = {

    # Misc
    holding = none

    # History

}

###c_essilvar
414 = {		#Essilvar

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2154 = {

    # Misc
    holding = none

    # History

}

##d_hawkfields
###c_hawkshot
410 = {		#Hawkshot

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2139 = {

    # Misc
    holding = none

    # History

}
2140 = {

    # Misc
    holding = none

    # History

}

###c_irmathmas
411 = {		#Irmathmas

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2136 = {

    # Misc
    holding = none

    # History

}
2137 = {

    # Misc
    holding = none

    # History

}
2138 = {

    # Misc
    holding = none

    # History

}

###c_birchwhite
409 = {		#Birchwhite

    # Misc
    culture = milcori
    religion = cult_of_nerat
	holding = tribal_holding

    # History
}
2135 = {

    # Misc
    holding = none

    # History

}
