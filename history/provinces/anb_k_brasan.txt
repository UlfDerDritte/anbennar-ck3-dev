#k_brasan
##d_sap_brasan
###c_brasan
565 = {		#Brasan

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_djinnkalis
564 = {		#Djinnkalis

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_rakkubtu
563 = {		#Rakkubtu

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_baru_brasan
###c_brasan_tanuz
570 = {		#Brasan Tanuz

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_kadumar
568 = {		#Kadumar

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_aklum
584 = {		#Aklum

    # Misc
    culture = barsibu
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_len_brasan
###c_medurubar
561 = {		#Medurubar

    # Misc
    culture = barsibu
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_suran_narit
572 = {		#Suran Narit

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_urasum
567 = {		#Urasum

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_aqatabtu
562 = {		#Aqatabtu

    # Misc
    culture = brasanni
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}