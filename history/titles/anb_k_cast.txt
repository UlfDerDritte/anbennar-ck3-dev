﻿
k_cast = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.1 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 89 # Wynstan sil Cast
	}
}

d_trialmount = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.1 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 89 # Wynstan sil Cast
	}
}

c_the_north_citadel = {
	1000.1.1 = { change_development_level = 12 }
	1020.10.1 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 89 # Wynstan sil Cast
	}
}

c_victors_path = {
	1020.10.31 = {
		liege = k_cast
		holder = trialwoud_0001
	}
}

c_glory_road = {
	1019.02.22 = {
		liege = e_castanor
		holder = nortmount_0001
	}
	1020.10.1 = {
		liege = k_cast
	}
}

d_nath = {
	1000.1.1 = { change_development_level = 12 }
}

c_aldenmore = {
	972.1.1 = {
		liege = e_castanor
		holder = 61007
	}
	1019.10.31 = {	# unspecified when Osric acquired the land, so I'll go with this for now
		liege = k_anor
		holder = 61001 # Osric of Aldenmore
	}
}

d_northyl = {
	1000.1.1 = { change_development_level = 8 }
}

c_nortwood = {	
	1020.10.31 = {
		liege = k_cast
		holder = giantswood_0001
	}
}

c_acenort = {
	1020.10.31 = {
		liege = k_cast
		holder = giantswood_0001
	}
}

c_hunters_folly = {
	1020.10.31 = {
		liege = k_cast
		holder = drachongard_0001
	}
}

c_khugsroad = {
	1000.1.1 = { change_development_level = 10 }
	1020.10.31 = {
		liege = k_cast
		holder = khugsarod_0001
	}
}

d_steelhyl = {
	1000.1.1 = { change_development_level = 9 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = k_cast
		holder = vanbury_steelhyl_0001 # Caylen of Vanbury-Steelhyl, given to him to revitialize Escanni economy + pay dividends to Free Realms
	}
}

d_westgate = {
	1000.1.1 = { change_development_level = 10 }
}

c_westgate = {
	1021.10.31 = {
		liege = k_cast
		holder = escanni_0016
	}
}

c_rigelham = { 
	1021.08.23 = {
		liege = k_cast
		holder = escanni_0017
	}
}

c_westwood = {
	1020.10.31 = {
		liege = k_cast
		holder = honeyaxe_0001
	}
}

d_serpentsmarck = {
	1010.02.22 = {
		liege = e_castanor
		holder = serpentsmarck_0006
	}
	1015.12.06 = {
		holder = serpentsmarck_0001
	}
	1020.02.22 = {
		liege = k_cast
	}
}

c_charwic = {
	1010.02.22 = {
		liege = d_serpentsmarck
		holder = serpentsmarck_0006
	}
	1015.12.06 = {
		holder = serpentsmarck_0001
	}
}

c_serpentswic = {
	1010.02.22 = {
		liege = d_serpentsmarck
		holder = serpentsmarck_0006
	}
	1015.12.06 = {
		holder = serpentsmarck_0001
	}
}

c_nortmarck = {
	1010.02.22 = {
		liege = d_serpentsmarck
		holder = serpentsmarck_0006
	}
	1015.12.06 = {
		holder = serpentsmarck_0005
	}
}

c_shatterwood = {
	1010.10.31 = { 
		liege = d_serpentsmarck
		holder = shatterwood_0002
	}
	1015.10.31 = { 
		holder = shatterwood_0003
	}
	1018.03.06 = { 
		holder = shatterwood_0004
	}
	1020.02.22 = {
		holder = shatterwood_0001
	}
}
